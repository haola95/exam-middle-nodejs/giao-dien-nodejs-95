// Câu lệnh này tương tự câu lệnh import express from 'express'; Dùng để import thư viện express vào project
const express = require('express');

// Import router
const router = require('./app/routes/router')

// Khởi tạo app express
const app = express();

// Khai báo cổng của project
const port = 8000;

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
  let today = new Date();

  response.status(200).json({
      message: `Xin chào Devcamp, hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
  })
})

app.use("/", router);

// Chạy app express
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

