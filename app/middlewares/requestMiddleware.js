const printRequestURLMiddleware = (request, response, next) => {
    console.log("Request URL: ", request.url);
    next();
}

module.exports = {
    printRequestURLMiddleware: printRequestURLMiddleware
}