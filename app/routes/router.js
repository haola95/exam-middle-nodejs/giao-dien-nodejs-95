// Import bo thu vien express
const express = require('express');

// Import 
const { printRequestURLMiddleware } = require("../middlewares/requestMiddleware.js")

const router = express.Router();

router.use((request, response, next) => {
    console.log("Time", new Date());
    next();
});

// Khai báo API dạng get "/" sẽ chạy vào đây
// Callback function: Là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
router.get("/router", printRequestURLMiddleware,  (request, response) => {
    response.status(200).redirect('https://getbootstrap.com/docs/4.0/examples/sign-in')
})

// Export dữ liệu 1 module
module.exports = router;